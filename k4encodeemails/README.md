Encode Emails plugin for Craft CMS
==================================

Plugin that allows you to encode emailaddresses in your CMS content.


Example
=======
Add to your template:

```
<p>{{ entry.body | encodeEmails | raw }}</p>
```

In this example, all emailaddresses in entry.body are encoded, both in text and mailto-links.