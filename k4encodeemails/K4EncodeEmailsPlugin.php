<?php
namespace Craft;
class K4EncodeEmailsPlugin extends BasePlugin
{
    public function getName()
    {
         return Craft::t('K4 Encode Emails');
    }
    public function getVersion()
    {
        return '1.0.0';
    }
    public function getDeveloper()
    {
        return 'stefan friedrich | friedrich@kreisvier.ch | kreisvier.ch';
    }
    public function getDeveloperUrl()
    {
        return 'http://kreisvier.ch';
    }
    public function getDocumentationUrl()
    {
        return 'https://bitbucket.org/kreisvierdigital/k4encodeemails';
    }    
    public function hasCpSection()
    {
        return false;
    }
    public function addTwigExtension()
    {
        Craft::import('plugins.k4encodeemails.twigextensions.EncodeEmailsTwigExtension');
        return new EncodeEmailsTwigExtension();
    }
}