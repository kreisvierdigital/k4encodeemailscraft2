<?php 
namespace Craft;
use Twig_Extension;
use Twig_Filter_Method;
class EncodeEmailsTwigExtension extends \Twig_Extension
{

    public function getName()
    {
        return 'EncodeEmails';
    }
    public function getFilters()
    {
        return array(
            'encodeEmails' => new Twig_Filter_Method($this, 'encodeEmails'),
        );
    }

    
    /**
     * Returns a string converted to html entities
     * http://goo.gl/LPhtJ
     * 
     * @param  string $string Value to be encoded
     * @return mixed          Returns a string converted to html entities
     */
    public function encodeHtmlEntities($treffer)
    {
        
        $email = filter_var(filter_var($treffer[0], FILTER_SANITIZE_EMAIL), FILTER_VALIDATE_EMAIL);
        if ($email !== false) {
            $string = mb_convert_encoding($email, 'UTF-32', 'UTF-8');
            $t = unpack("N*", $string);
            $t = array_map(function($n) { return "&#$n;"; }, $t);
            return implode("", $t);
        } else {
            return($treffer[0]);
        }
        
    }
    
    
    public function encodeEmails($textstring)
    {

        $patternEmail = "/(?:[A-Za-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+=?^_`{|}~-]+)*|\"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*\")@(?:(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[A-Za-z0-9-]*[A-Za-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/";

        $textstring = preg_replace_callback($patternEmail, 
                                            function($treffer){ 
                                                return $this->encodeHtmlEntities($treffer);
                                            },
                                            $textstring);        

        return($textstring);

    }
    
}